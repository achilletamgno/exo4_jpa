package org.name;

import javax.persistence.Embeddable;


@Embeddable
public class Adresse {
	
	
	private String rue;
	private String commune;
	
	
	//GETTERS & SETTERS
	public String getRue() {
		return rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}
	public String getCommune() {
		return commune;
	}
	public void setCommune(String commune) {
		this.commune = commune;
	}

	//Affichage
	@Override
	public String toString() {
		return "Adresse [rue=" + rue + ", commune=" + commune + "]";
	}


}
