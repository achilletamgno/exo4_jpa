package org.name;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;

					/**ENTITY COMMUNE**/

@Entity
public class Commune {
	
	@Column(name="ID")
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name= "nom_commune" ,length=40) 
	private String NomCommune;
	
	@Column(name= "code_postal" ,length=40) 
	private String CodePostal;
	
	//Creation d'une relation entre Commune & Maire
	@OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinFetch(JoinFetchType.OUTER)
	private Maire maire;
	
	@OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinFetch(JoinFetchType.OUTER)
	private Departement departement;
	
	@Embedded
	private Adresse adresse;
	
	/*GETTERS & SETTERS*/
	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	/*GETTER & SETTER*/
	public Departement getDepartement() {
		return departement;
	}

	public void setDepartement(Departement departement) {
		this.departement = departement;
	}

	public Maire getMaire() {
		return maire;
	}

	public void setMaire(Maire maire) {
		this.maire = maire;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNomCommune() {
		return NomCommune;
	}

	public void setNomCommune(String nomCommune) {
		NomCommune = nomCommune;
	}

	public String getCodePostal() {
		return CodePostal;
	}

	public void setCodePostal(String codePostal) {
		CodePostal = codePostal;
	}
	
	
	//AFFICHAGE
	@Override
	public String toString() {
		return "Commune [le id=" + id + ", Nom de la Commune=" + NomCommune
				+ ", CodePostal=" + CodePostal + ", maire=" + maire
				+ ", departement=" + departement + "]";
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((CodePostal == null) ? 0 : CodePostal.hashCode());
		result = prime * result
				+ ((NomCommune == null) ? 0 : NomCommune.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((maire == null) ? 0 : maire.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Commune other = (Commune) obj;
		if (CodePostal == null) {
			if (other.CodePostal != null)
				return false;
		} else if (!CodePostal.equals(other.CodePostal))
			return false;
		if (NomCommune == null) {
			if (other.NomCommune != null)
				return false;
		} else if (!NomCommune.equals(other.NomCommune))
			return false;
		if (id != other.id)
			return false;
		if (maire == null) {
			if (other.maire != null)
				return false;
		} else if (!maire.equals(other.maire))
			return false;
		return true;
	}

}
