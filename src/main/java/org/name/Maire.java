package org.name;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;


@Entity
public class Maire {

	@Column(name="ID")
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name= "nom_maire" ,length=50) 
	private String NomMaire;

	//Creation d'une relation entre Communes & Maires
	@OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinFetch(JoinFetchType.OUTER)
	private Commune commune;


	//Adresse
	@Embedded
	private Adresse adresse;

	//GETTERS & SETTERS
	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	//GETTERS & SETTERS
	public Commune getCommune() {
		return commune;
	}

	public void setCommune(Commune commune) {
		this.commune = commune;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNomMaire() {
		return NomMaire;
	}

	public void setNomMaire(String nomMaire) {
		NomMaire = nomMaire;
	}

	@Override
	public String toString() {
		return "Maire [son id=" + id + ", Nom du Maire=" + NomMaire + ", La commune="
				+ commune + "]";
	}
}
